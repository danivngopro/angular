import { Component } from '@angular/core';
import { cardInterface } from './card-creator/interfaces/cardInterface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  card: cardInterface;
  index: number = 0;

  onClickSubmit(newData: cardInterface) {
    this.card = newData;
    this.card.index = this.index;
    this.index++;
  }



}
