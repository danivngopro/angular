import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardCreatorComponent } from './card-creator/card-creator.component';
import {FormsModule} from '@angular/forms';
import { CardPrinterComponent } from './card-printer/card-printer.component'


@NgModule({
  declarations: [
    AppComponent,
    CardCreatorComponent,
    CardPrinterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
