import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';
import { cardInterface } from '../card-creator/interfaces/cardInterface';

@Component({
  selector: 'app-card-printer',
  templateUrl: './card-printer.component.html',
  styleUrls: ['./card-printer.component.css']
})
export class CardPrinterComponent implements OnInit {

  @Input() card:cardInterface;

  constructor() { }

  ngOnInit(): void {
  }

  sendDeleteOrder() {
    //send index of current card to card creator.
    this.deleteOrder.emit(this.card.index);
  }

  @Output() deleteOrder:EventEmitter<any> = new EventEmitter<number>();

}
