import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardPrinterComponent } from './card-printer.component';

describe('CardPrinterComponent', () => {
  let component: CardPrinterComponent;
  let fixture: ComponentFixture<CardPrinterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardPrinterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardPrinterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
