import { findReadVarNames } from '@angular/compiler/src/output/output_ast';
import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import {cardInterface} from './interfaces/cardInterface';

@Component({
  selector: 'app-card-creator',
  templateUrl: './card-creator.component.html',
  styleUrls: ['./card-creator.component.css']
})

export class CardCreatorComponent implements OnInit {
  
  cards: cardInterface[] = [];
  
  @Input('card')
  set card(card:cardInterface) {
    if(card) {
      this.cards.push(card);
    }
  }

  deleteCard(val) {
    if(val!=4) {
      for (let cardIndex = 0; cardIndex < this.cards.length; cardIndex++) {
        if(this.cards[cardIndex].index === val) {
          this.cards.splice(cardIndex,1);
        }
      }
    }
  }

  ngOnInit(): void {
  }
  
}
